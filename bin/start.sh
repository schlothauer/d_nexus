#!/bin/bash

scriptPos=${0%/*}

source "$scriptPos/image_conf.sh"


aktImgName=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $1}'`
aktImgVers=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $2}'`

HostIP=`ifconfig | grep 'inet ' | grep Bcast | grep 192 | awk '{print $2}' | sed 's/addr://' | sed 's/Adresse://'`

if [ "$aktImgName" == "$imageBase" ] && [ "$aktImgVers" == "$imageTag" ]
then
        echo "run container from image: $aktImgName:$aktImgVers"
else
    if docker build -t $imageName $scriptPos/../image
    then
        echo -en "\033[1;34m  image created: $1 \033[0m\n"
    else
        echo -en "\033[1;31m  error while create image: $imageName \033[0m\n"
        exit 1
    fi
fi

tmpDataDir=$scriptPos/../tmp/data
if ! [ -d $tmpDataDir ]; then
    mkdir -p $tmpDataDir
fi

absDataPath=`pushd $tmpDataDir > /dev/null && pwd && popd > /dev/null`

docker run -d -p 2379:2379 \
              -p 2380:2380 \
              -p 4001:4001 \
              -p 7001:7001 \
              -v $absDataPath:/data \
              --name etcd-test-cont $imageName \
              -name etcd0 \
              -advertise-client-urls http://$HostIP:2379,http://$HostIP:4001 \
              -initial-advertise-peer-urls http://$HostIP:2380 \
              -initial-cluster-token etcd-cluster-1 \
              -initial-cluster etcd0=http://$HostIP:2380 \
              -initial-cluster-state new

